<?php

include_once 'Product.php';
include_once __DIR__ . "/../database/database.php";

class Furniture extends Product{
    private $height;
    private $width;
    private $length;
    
    public function __construct($data){
        parent::__construct();
        $this->setName($data['name']);
        $this->setPrice($data['price']);
        $this->setSKU($data['sku']);
        $this->setWidth($data['width']);
        $this->setLength($data['length']);
        $this->setHeight($data['height']);
        $this->store();
    }

    public function store(){
        $sql = "INSERT INTO products (sku,name,price,width,length,height) VALUES (?,?,?,?,?,?)";
        $this->db->getConnection()->prepare($sql)->execute([$this->sku,$this->name,$this->price,$this->width,$this->length,$this->height]);

    }

    private function setHeight($height){
        $this->isEmpty($height,'height');
        $this->isNumeric($height,'height');

        $this->$height = $height;

    }

    private function setWidth($width){
        $this->isEmpty($width,'width');
        $this->isNumeric($width,'width');

        $this->$width = $width;

    }

    private function setLength($length){
        $this->isEmpty($length,'length');
        $this->isNumeric($length,'length');

        $this->$length = $length;

    }

    
    private function getHeight(){
        return $this->height;
    }

    private function getWidth(){
        return $this->width;
    }
    private function getLength(){
        return $this->length;
    } 
}