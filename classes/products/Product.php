<?php
include_once __DIR__ . "/../database/database.php";

 class Product {

    protected $sku;
    protected $name;
    protected $price;
    protected $db;

    public function __construct() {
            $this->db = new Database();
            
    }


    protected function setName($name){
        $this->isEmpty($name,'name');
        $this->name = $name;

    }

    protected function setPrice($price){
        $this->isEmpty($price,'price');
        $this->isNumeric($price,'price');
        
        $this->price = $price;

    }

    protected function setSKU($sku){
        $this->isEmpty($sku,'SKU');
            $obj = new Database();
            $statement = $obj->getConnection()->prepare("select id from products where sku = :sku");
            $statement->execute(array(':sku' => $sku));
            $result = $statement->fetchAll(PDO::FETCH_OBJ);
            if ($result){
                setcookie('msg', 'only unique skus allowed', time() + (86400 * 30), "/");
                header("Location:/scandi/add-product.php");
                die();
        }


        $this->sku = $sku;

    }


    protected function getName(){
        return $this->name;
    }

    protected function getPrice(){
        return $this->price;
    }

    protected function getSKU(){
        return $this->sku;
    }


    public  function getProducts(){
        $query = "SELECT * FROM products";

        $result = $this->db->getConnection()->query($query);
        $data    = $result->fetchAll(PDO::FETCH_OBJ);
        if ($data){
            return $data;
        }
        else{
            return array();
        }
        
    }
    public function delete($id){
        $sql = "DELETE FROM products WHERE id=?";
        $stmt= $this->db->getConnection()->prepare($sql);
        $stmt->execute([$id]);
    }

    protected function isEmpty($val, $fieldName){
        
            if(empty($val)){
            $_SESSION['error'] = $fieldName . 'cant be empty';
            
            header("Location:/scandi/add-product.php");
            die();

        }
    }
    
    protected function isNumeric($val,$fieldName){
        if(!is_numeric($val)){
            setcookie('error', $fieldName.'must be numeric', time() + (86400 * 30), "/");
            
            header("Location:/scandi/add-product.php");
            die();
        }
    }

}
