<?php 
    class Database {
        private $user = "root";
        private $pass = "";
        private $database = "scandi";
        private $dbh = null;

        public function __construct() {
            $this->dbh = new PDO("mysql:host=localhost;dbname=" . $this->database . ";", $this->user, $this->pass);
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }

        public function getConnection() {
            return $this->dbh;
        }

        

        public function create_table(){
            $sql = "CREATE TABLE `test` ( `id` INT NOT NULL AUTO_INCREMENT , `sku` VARCHAR(255) NOT NULL , `name` VARCHAR(255) NOT NULL , `price` FLOAT NOT NULL , `weight` INT NULL DEFAULT NULL , `size` INT NULL DEFAULT NULL , `length` INT NULL DEFAULT NULL , `width` INT NULL DEFAULT NULL , `height` INT NULL DEFAULT NULL , PRIMARY KEY (`id`), UNIQUE (`sku`))" ;
            $this->getConnection()->exec($sql);

        }
    }