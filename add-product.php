<?php session_start(); 
    include_once __DIR__ . "/save.php" ;
    ?>

<html>

<head>
  <title>Product add</title>
  <link rel="stylesheet" href="style.css">
<script>
  
  function prodType(prod) {
        var DVDAttributes = document.getElementById("dvd_attributes");
        var FurnitureAttributes = document.getElementById("furniture_attributes");
        var BookAttributes = document.getElementById("book_attributes");

      DVDAttributes.style.display = "none";
      FurnitureAttributes.style.display = "none";
      BookAttributes.style.display = "none";
      var obj = {};
      obj['DVD'] = function() {
        DVDAttributes.style.display = "block";

      }
      obj['Furniture'] = function(){
        FurnitureAttributes.style.display = "block";
      }

      obj['Book'] = function(){
        BookAttributes.style.display = "block";
      }

     obj[prod]();
    }

</script>  
</head>

<?php 

if ($_SERVER["REQUEST_METHOD"] == "POST"){
  $data = $_POST;  
  $obj = new Save($data);

}
?>

<body>
  




  <div class="ProductAdd">
    <h2>
      <b>Product Add</b>
      

      <button onclick="window.location.href='/scandi'">Cancel</button>
    </h2>
    <hr>
  </div>  

  <div class="form">
    <label id="Error"></label>


    <form  id="product_form" method="POST" action="/scandi/add-product">
      <?php
      
      if (isset($_COOKIE['msg'])) {
        echo $_COOKIE['msg'];
        setcookie('msg', null, -1, '/');  


      }

      if (isset($_COOKIE['error'])) {
        echo $_COOKIE['error'];
        setcookie('error', null, -1, '/');  

      }
      ?>

      <br><br>
      <label for="SKU">SKU</label>
      <input type="text" id="sku" name="sku"><br>
      <label for="Name ">Name</label>
      <input type="text" id="name" name="name"><br>
      <label for="Price">Price</label>
      <input type="number" step="0.01" min=0 id="price" name="price"><br>
      <label>Type Switcher</label>
      <select id="productType" name="product" onChange="prodType(this.value);">
        <option value="">Choose Type</option>
        <option value="DVD">Dvd</option>
        <option value="Furniture">Furniture</option>
        <option value="Book">Book</option>
      </select>

      <div class="fieldbox" id="dvd_attributes">
        <label>Size</label>
        <input type="number" id="size" min=0 name="size" value="">
        please provide size

      </div>

      <div class="fieldbox" id="book_attributes">
        <label>Weight</label>
        <input type="number" name="weight" min=0 id="weight" value="">
        please provide weight

      </div>

      <div class="fieldbox" id="furniture_attributes">
        <label>Length</label>
        <input type="number" name="length" min=0 id="length" value=""><br>
        <label>Width</label>
        <input type="number" name="width"  min=0 id="width" value=""><br>
        <label>Height</label>
        <input type="number" name="height" min=0 id="height" value=""><br>
        please provide H x W x L
      </div>
      <input type="submit" name="submit" id="submit" class="button" value="Save" />

    </form>


  </div>

<?php 
//include   __DIR__ . "/validation.html" ;
?>

</body>

</html>