<?php

include_once 'Product.php';
include_once __DIR__ . "/../database/database.php";

class Book extends Product{
    private $weight;

       public function __construct($data){
        parent::__construct();
        $this->setName($data['name']);
        $this->setPrice($data['price']);
        $this->setSKU($data['sku']);
        $this->setWeight($data['weight']);
        $this->store();
    }

    public function store(){
        $sql = "INSERT INTO products (sku,name,price,weight) VALUES (?,?,?,?)";
        $this->db->getConnection()->prepare($sql)->execute([$this->sku,$this->name,$this->price,$this->weight]);
        
    }

    public function setWeight($weight){
        $this->isEmpty($weight,'weight');
        $this->isNumeric($weight,'weight');
        $this->weight = $weight;

    }


    public function getWeight(){
        return $this->weight;
    }

}