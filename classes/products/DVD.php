<?php

include_once 'Product.php';
include_once __DIR__ . "/../database/database.php";

class DVD extends Product
{
    private $size;

    public function __construct($data)
    {
        parent::__construct();
        $this->setName($data['name']);
        $this->setPrice($data['price']);
        $this->setSKU($data['sku']);
        $this->setSize($data['size']);
        $this->store();
    }

    public function store()
    {
        $sql = "INSERT INTO products (sku,name,price,size) VALUES (?,?,?,?)";
        $this->db->getConnection()->prepare($sql)->execute([$this->sku, $this->name, $this->price, $this->size]);
    }

    public function setSize($size)
    {
        $this->isEmpty($size, 'size');
        $this->isNumeric($size,'size');
        $this->size = $size;
    }


    public function getSize()
    {
        
        return $this->size;
    }
}
